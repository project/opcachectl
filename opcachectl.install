<?php

use Drupal\Core\Link;
use Drupal\Core\Site\Settings;

// TODO: remove Drupal core "PHP OPCODE CACHING" requirement.
// This module provides a replacement for the core's requirement...

/**
 * Implements hook_requirements().
 */
function opcachectl_requirements($phase) {
  $requirements = [];

  $opcache_extension_loaded = extension_loaded('Zend OPcache');

  if ($phase == 'runtime') {
    if ($opcache_extension_loaded) {
      $opcache_config = opcache_get_configuration();
      $opcache_version = $opcache_config['version'];
      $opcache_config = $opcache_config['directives'];

      $requirements['opcache_loaded']['title'] = t('OPcache PHP Extension');
      $requirements['opcache_loaded']['severity'] = REQUIREMENT_OK;
      $requirements['opcache_loaded']['value'] = t('@name loaded (@version)', [
        '@name' => $opcache_version['opcache_product_name'],
        '@version' => $opcache_version['version'],
      ]);

      if ($opcache_config['opcache.enable']) {
        $requirements['opcache_enabled']['title'] = t('OPcache enabled');
        $requirements['opcache_enabled']['severity'] = REQUIREMENT_OK;
        $requirements['opcache_enabled']['value'] = t('Yes');

        $requirements['opcache_enabled']['description'] = Link::createFromRoute(t('see configuration'), 'opcachectl.report.config')
          ->toString();

        $token = Settings::get("opcachectl_reset_token");
        $remote_addresses = Settings::get('opcachectl_reset_remote_addresses', []);
        if (is_array($remote_addresses)) {
          $remote_addresses = implode(", ", $remote_addresses);
        }
        $desc = "";
        $requirements['opcache_reset_token']['title'] = t('opcachectl remote reset');
        $requirements['opcache_reset_token']['value'] = "";
        if (!empty($token) || !empty($remote_addresses)) {
          $requirements['opcache_reset_token']['severity'] = REQUIREMENT_OK;
        }
        else {
          $requirements['opcache_reset_token']['severity'] = REQUIREMENT_INFO;
          $requirements['opcache_reset_token']['value'] .= t('Remote reset of PHP OPcache is NOT ENABLED.');
        }
        if (!empty($remote_addresses)) {
          $requirements['opcache_reset_token']['value'] .= t('Remotely resetting PHP OPcache is ALLOWED from address(es) :addresses without a token.' . PHP_EOL, [":addresses" => $remote_addresses]);
        }
        else {
          $desc .= t('Define allowed remote addresses in $settings["opcachectl_reset_remote_addresses"] to reset PHP OPcache without a token.' . PHP_EOL);
        }
        if (!empty($token)) {
          $requirements['opcache_reset_token']['value'] .= t('TOKEN to rest PHP OPcache remotely from any address ENABLED in $settings["opcachectl_reset_token"].' . PHP_EOL);
        }
        else {
          $desc .= t('Define a token to remotely reset PHP OPcache form any address in $settings["opcachectl_reset_token"].' . PHP_EOL);
        }
        $requirements['opcache_reset_token']['description'] = $desc;
        //        $requirements['opcache_reset_token']['description'] = t('A token may be defined in settings.php ($settings["opcachectl_reset_token"]) to allow resetting PHP OPcache via <code>GET :route</code>.', [':route' => $opcache_reset_path]);

      }
      else {
        $requirements['opcache_enabled']['title'] = t('OPcache enabled');
        $requirements['opcache_enabled']['severity'] = REQUIREMENT_WARNING;
        $requirements['opcache_enabled']['value'] = t('No');
      }
    }
    else {
      $requirements['opcache_loaded']['title'] = t('OPcache PHP Extension');
      $requirements['opcache_loaded']['severity'] = REQUIREMENT_WARNING;
      $requirements['opcache_loaded']['value'] = t('Zend OPcache not loaded');
    }
  }

  return $requirements;
}
